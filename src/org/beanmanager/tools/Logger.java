/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.tools;

public class Logger {
	private static boolean[] logInfo = new boolean[7];
	public static final int RUNTIME = 0;
	public static final int RMI = 1;
	public static final int SAVE_OR_LOAD_FILE = 2;
	public static final int BENCHMARK = 3;
	public static final int MODULE = 4;
	public static final int LIBRARY_LOADING = 5;
	public static final int DYNAMICIO = 6;

	private Logger() {}

	public static void setLevel(int type, boolean log) {
		Logger.logInfo[type] = log;
	}

	public static void log(int type, String message) {
		if (logInfo[type])
			System.out.println(message);
	}

	public static void logError(int type, String message) {
		System.err.println(message);
	}
}
