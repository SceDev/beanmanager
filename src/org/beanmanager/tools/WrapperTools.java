/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.tools;

import java.util.List;

public class WrapperTools {
	public static final List<Class<?>> PRIMITIVES = List.of(boolean.class, byte.class, double.class, float.class, int.class, long.class, short.class);
	public static final List<Class<?>> WRAPPERS = List.of(Boolean.class, Byte.class, Double.class, Float.class, Integer.class, Long.class, Short.class);

	private WrapperTools() {}

	public static Class<?> toPrimitive(Class<?> type) {
		for (int i = WRAPPERS.size(); i-- != 0;)
			if (type == WRAPPERS.get(i))
				return PRIMITIVES.get(i);
		return type;
	}

	public static Class<?> toWrapper(Class<?> type) {
		for (int i = PRIMITIVES.size(); i-- != 0;)
			if (type == PRIMITIVES.get(i))
				return WRAPPERS.get(i);
		return type;
	}
}
