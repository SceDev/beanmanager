package org.beanmanager.testbean;

import org.beanmanager.BeanManager;
import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.container.BeanInfo;
import org.beanmanager.ihmtest.FxTest;

import javafx.scene.layout.VBox;

public class BeanPropertyTest {
	@PropertyInfo(nullable = true)
	@BeanInfo(inline = true)
	private SubCarotte inlineCarotte;
	@PropertyInfo(nullable = false)
	@BeanInfo(inline = true)
	private SubCarotte inlineNonNullableCarotte;
	
	@PropertyInfo(nullable = true)
	@BeanInfo(inline = true, possibleSubclasses = {MoutonBlanc.class, MoutonNoir.class})
	private AbstractMouton inlineMouton;
	@PropertyInfo(nullable = false)
	@BeanInfo(inline = true, possibleSubclasses = {MoutonBlanc.class, MoutonNoir.class})
	private AbstractMouton inlineNonNullableMouton;
	
	private SubCarotte sharedCarotte;
	
	public static void main(String[] args) {
		new FxTest().launchIHM(args, s -> new VBox(new BeanManager(new BeanPropertyTest(), "").getEditor()), true);
	}

	public SubCarotte getInlineCarotte() {
		return inlineCarotte;
	}

	public void setInlineCarotte(SubCarotte inlineCarotte) {
		this.inlineCarotte = inlineCarotte;
	}

	public SubCarotte getInlineNonNullableCarotte() {
		return inlineNonNullableCarotte;
	}

	public void setInlineNonNullableCarotte(SubCarotte inlineNonNullableCarotte) {
		this.inlineNonNullableCarotte = inlineNonNullableCarotte;
	}

	public SubCarotte getSharedCarotte() {
		return sharedCarotte;
	}

	public void setSharedCarotte(SubCarotte sharedCarotte) {
		this.sharedCarotte = sharedCarotte;
	}

	public AbstractMouton getInlineMouton() {
		return inlineMouton;
	}

	public void setInlineMouton(AbstractMouton inlineMouton) {
		this.inlineMouton = inlineMouton;
	}

	public AbstractMouton getInlineNonNullableMouton() {
		return inlineNonNullableMouton;
	}

	public void setInlineNonNullableMouton(AbstractMouton inlineNonNullableMouton) {
		this.inlineNonNullableMouton = inlineNonNullableMouton;
	}
}
