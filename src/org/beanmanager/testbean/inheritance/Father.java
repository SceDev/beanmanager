/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.testbean.inheritance;

public class Father {
	private int patate;

	public int getPatate() {
		return patate;
	}

	public void setPatate(int patate) {
		this.patate = patate;
	}
}
