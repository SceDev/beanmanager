/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.testbean.inheritance;

public class Child extends Father{	
	private Child() {}
	
	private int ours;

	public int getOurs() {
		return ours;
	}

	public void setOurs(int ours) {
		this.ours = ours;
	}
}
