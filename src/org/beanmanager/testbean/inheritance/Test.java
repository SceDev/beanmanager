/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.testbean.inheritance;

import org.beanmanager.BeanManager;
import org.beanmanager.ihmtest.FxTest;

import javafx.scene.layout.VBox;

public class Test {
	public static void main(String[] args) {
		BeanManager bm = new BeanManager(new Test(), "");
		new FxTest().launchIHM(args, s -> new VBox(bm.getEditor()));
	}
	
	private Child child;

	public Child getChild() {
		return child;
	}

	public void setChild(Child child) {
		this.child = child;
	}
}
