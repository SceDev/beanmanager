/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.basic;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.beanmanager.editors.PropertyEditor;
import org.beanmanager.ihmtest.FxTest;
import org.beanmanager.tools.FxUtils;

import javafx.scene.control.Control;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;

public class StringEditor extends PropertyEditor<String> {
	private String style;
	private int maxLength = -1;
	private String promptText;

	public static void main(String[] args) {
		new FxTest().launchIHM(args, s -> {
			StringEditor se = new StringEditor();
			se.setPossibilities(new String[] { "ours", "dauphin", "vache" });
			return new VBox(new StringEditor().getEditor(), se.getEditor());
		});
	}

	public StringEditor() {}

	@Override
	public boolean canContainForbiddenCharacter() {
		return true;
	}

	@Override
	public String getAsText() {
		return getValue();
	}

	@Override
	protected Control getCustomEditor() {
		TextField textField = new TextField(getValue());
		textField.setPromptText(this.promptText);
		textField.setPrefWidth(150);
		textField.focusedProperty().addListener((ov, oldValue, newValue) -> {
			if (!newValue)
				updateValue();
		});
		textField.setOnScroll(e -> {
			if (e.getDeltaY() == 0) {
				int index = textField.getCaretPosition() + (e.getDeltaY() > 0 ? -1 : 1);
				if (textField.getText() != null)
					textField.positionCaret(index < 0 ? 0 : index >= textField.getText().length() ? textField.getText().length() : index);
			}
			e.consume();
		});
		textField.textProperty().addListener((ov, oldValue, newValue) -> {
			if (this.maxLength > 0 && newValue != null && newValue.length() > this.maxLength)
				textField.setText(oldValue);
			updateStyle();
		});
		textField.setMaxWidth(Double.MAX_VALUE);
		textField.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.ENTER) {
				updateValue();
				FxUtils.traverse();
				e.consume();
			}
		});
		addPropertyChangeListener(() -> updateGUI());
		if (this.style != null)
			textField.setStyle(this.style);
		return textField;
	}

	@Override
	public int getPitch() {
		return -1;
	}

	@Override
	public boolean hasCustomEditor() {
		return true;
	}

	@Override
	public boolean isFixedControlSized() {
		return true;
	}

	@Override
	public String readValue(DataInput raf) throws IOException {
		return readString(raf);
	}

	@Override
	public void setAsText(String text) {
		setValue(text);
	}

	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
		String text = getValue();
		if (maxLength > 0 && text != null && text.length() > maxLength)
			setValue(text.substring(0, maxLength));
	}

	public void setPromptText(String promptText) {
		this.promptText = promptText;
		TextField textField = (TextField) customEditor();
		if (textField != null)
			textField.setPromptText(promptText);
	}

	public void setStyle(String style) {
		this.style = style;
	}

	@Override
	public void updateCustomEditor() {
		TextField textField = (TextField) customEditor();
		if (textField != null) {
			textField.setText(getValue());
			updateStyle();
		}
	}

	protected void updateStyle() {
		TextField textField = (TextField) customEditor();
		String style = "-fx-text-fill: " + (getValue() == null || textField.getText().equals(getValue()) ? "black" : "green") + ";";
		if (this.style != null)
			style = this.style + style;
		if (!style.equals(textField.getStyle()))
			textField.setStyle(style);
	}

	private void updateValue() {
		TextField textField = (TextField) customEditor();
		if (textField != null) {
			textField.setStyle("-fx-text-fill: black;");
			setValue(textField.getText());
		}
	}

	@Override
	public void writeValue(DataOutput raf, String value) throws IOException {
		writeString(raf, value);
	}

	public static String readString(DataInput raf) throws IOException {
		int size = raf.readInt();
		if (size < 0)
			return null;
		byte[] b = new byte[size];
		raf.readFully(b);
		return new String(b, StandardCharsets.UTF_8);
	}

	public static void writeString(DataOutput raf, String string) throws IOException {
		if (string == null)
			raf.writeInt(-1);
		else {
			byte[] b = string.getBytes(StandardCharsets.UTF_8);
			raf.writeInt(b.length);
			raf.write(b);
		}
	}
}
