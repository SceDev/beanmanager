/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.basic;

import org.beanmanager.editors.PropertyEditor;
import org.beanmanager.editors.primitive.BooleanEditor;
import org.beanmanager.struct.BooleanProperty;

import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;

public class BooleanPropertyEditor extends PropertyEditor<BooleanProperty> {
	private final BooleanEditor be = new BooleanEditor();

	public BooleanPropertyEditor() {
		this.be.addPropertyChangeListener(() -> {
			BooleanProperty val = getValue();
			if (val == null) {
				val = new BooleanProperty("", this.be.getValue());
				setValue(val);
			}
			val.value = this.be.getValue();
			firePropertyChangeListener();
		});
	}

	@Override
	public String getAsText() {
		if (getValue() == null)
			return "";
		String propertyName = getValue().name;
		if (propertyName == null || propertyName.isEmpty())
			propertyName = "bool";
		return propertyName + " " + this.be.getAsText();
	}

	@Override
	protected Region getCustomEditor() {
		String propertyName = getValue() == null ? "" : getValue().name;
		if (propertyName == null)
			propertyName = "bool";
		addPropertyChangeListener(() -> updateGUI());
		return new HBox(this.be.getNoSelectionEditor(), new Label(propertyName));
	}

	@Override
	public boolean hasCustomEditor() {
		return true;
	}

	@Override
	public void setAsText(String text) {
		int idSplit = text.lastIndexOf(" ");
		setValue(new BooleanProperty(text.substring(0, idSplit), false));
		this.be.setAsText(text.substring(idSplit + 1));
	}

	@Override
	public void setValue(BooleanProperty value) {
		super.setValue(value);
		if (value == null)
			return;
		this.be.setValue(value.value);
	}

	@Override
	public String toString() {
		return (getValue() == null ? "" : getValue().name) + ": " + this.be.getAsText();
	}

	@Override
	public void updateCustomEditor() {
		this.be.setValue(getValue().value);
	}
}
