/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.primitive;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.beanmanager.ihmtest.FxTest;
import org.beanmanager.tools.FxUtils;

import javafx.geometry.Pos;
import javafx.scene.control.CheckBox;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;

public class BooleanEditor extends PrimitiveEditor<Boolean> {

	public static void main(String[] args) {
		new FxTest().launchIHM(args, s -> {
			VBox vbox = new VBox(new BooleanEditor().getCustomEditor());
			vbox.setAlignment(Pos.CENTER);
			return vbox;
		});
	}

	@Override
	public String getAsText() {
		Boolean val = getValue();
		return val == null ? null : Boolean.toString(val);
	}

	@Override
	protected CheckBox getCustomEditor() {
		if (getValue() == null && this.primitive)
			setValue(false);
		CheckBox checkBox = new CheckBox();
		checkBox.setAllowIndeterminate(!this.primitive && nullable());
		if (getValue() == null)
			checkBox.setIndeterminate(true);
		else {
			checkBox.setSelected(getValue());
			checkBox.setIndeterminate(false);
		}
		checkBox.setOnAction(e -> setValue(checkBox.isIndeterminate() ? null : checkBox.isSelected()));
		// checkBox.setIndeterminate(!primitive);
		checkBox.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.ENTER) {
				FxUtils.traverse();
				e.consume();
			}
		});
		return checkBox;
	}

	@Override
	public int getPitch() {
		return 1;
	}

	@Override
	public boolean hasCustomEditor() {
		return true;
	}

	@Override
	public boolean isFixedControlSized() {
		return true;
	}

	@Override
	public Boolean readValue(DataInput raf) throws IOException {
		return raf.readBoolean();
	}

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		setValue(text == null || text.isEmpty() ? null : Boolean.parseBoolean(text));
	}

	@Override
	public void updateCustomEditor() {
		CheckBox cb = (CheckBox) customEditor();
		if (getValue() == null)
			cb.setIndeterminate(true);
		else
			cb.setSelected(getValue());
	}

	@Override
	public void writeValue(DataOutput raf, Boolean value) throws IOException {
		raf.writeBoolean(value);
	}
}
