/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.primitive.number;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.beanmanager.ihmtest.FxTest;

import javafx.scene.layout.VBox;

public class ShortEditor extends NumberEditor<Short> {
	private short min = getMinTypeValue();
	private short max = getMaxTypeValue();

	public static void main(String[] args) {
		new FxTest().launchIHM(args,
				s -> new VBox(new ShortEditor((short) 10, (short) 20, ControlType.TEXTFIELD).getEditor(), new ShortEditor((short) 0, (short) 1000, ControlType.SPINNER_AND_SLIDER).getEditor()));
	}

	public ShortEditor() {}

	public ShortEditor(short min, short max, ControlType controlType) {
		setMin(min);
		setMax(max);
		setControleType(controlType);
	}

	public ShortEditor(short min, short max, ControlType controlType, short increment, IncrementMode incrementMode) {
		setMin(min);
		setMax(max);
		setIncrement(increment);
		setIncrementMode(incrementMode);
		setControleType(controlType);
	}

	public ShortEditor(Short[] numbers) {
		super();
		setPossibilities(numbers);
	}

	@Override
	protected Short changeValue(Short incrFactor, boolean positive, boolean ctrl, boolean shift) {
		short inc = incrFactor;
		if (shift && ctrl)
			inc *= 100;
		else if (shift)
			inc *= 1000;
		else if (ctrl)
			inc *= 10;
		return (short) (getValue() + (positive ? inc : -inc));
	}

	private void checkBounds() {
		// if (max == Short.MIN_VALUE)
		// max++;
		// if (min == Short.MAX_VALUE)
		// min--;
		if (this.max < this.min)
			this.max = this.min;
		updateValueWithBounds();
	}

	@Override
	protected Short defaultValue() {
		return this.min > 0 || this.max < 0 ? this.min : 0;
	}

	@Override
	public String getAsText() {
		return getValue() == null ? null : Short.toString(getValue());
	}

	@Override
	public Short getMax() {
		return this.max;
	}

	@Override
	public Short getMin() {
		return this.min;
	}

	@Override
	public Short getNumberAsGeneric(Number number) {
		return number.shortValue();
	}

	@Override
	protected String getNumberAsText(Short number) {
		return Short.toString(number.shortValue());
	}

	@Override
	public int getPitch() {
		return Short.BYTES;
	}

	@Override
	protected int isInBound(Short val) {
		return val < this.min ? -1 : val > this.max ? 1 : 0;
	}

	@Override
	protected boolean isIntegerNumber() {
		return true;
	}

	@Override
	protected Short parseNumber(String text) {
		return Short.parseShort(text);

	}

	@Override
	public Short readValue(DataInput raf) throws IOException {
		return raf.readShort();
	}

	@Override
	public void setAsText(String text) {
		setValue(text == null || text.isEmpty() || text.equals(String.valueOf((Object) null)) ? null : Short.parseShort(text));
	}

	@Override
	public void setMax(Short max) {
		this.max = max == null ? Short.MAX_VALUE : max;
		checkBounds();
		resetEditor();
	}

	@Override
	public void setMin(Short min) {
		this.min = min == null ? Short.MIN_VALUE : min;
		checkBounds();
		resetEditor();
	}

	@Override
	public void writeValue(DataOutput raf, Short value) throws IOException {
		raf.writeShort(value);
	}

	@Override
	public Short getMaxTypeValue() {
		return Short.MAX_VALUE;
	}

	@Override
	public Short getMinTypeValue() {
		return Short.MIN_VALUE;
	}
}
