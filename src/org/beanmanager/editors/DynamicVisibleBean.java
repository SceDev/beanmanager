/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors;

import java.util.WeakHashMap;

import javax.swing.event.EventListenerList;

import org.beanmanager.tools.FxUtils;

public interface DynamicVisibleBean {
	static WeakHashMap<Object, EventListenerList> LISTENERS = new WeakHashMap<>();

	public static void addVisibleChangeListener(Object bean, DynamicVisibleBeanPropertyListener listener) {
		EventListenerList list = LISTENERS.get(bean);
		if (list == null) {
			list = new EventListenerList();
			LISTENERS.put(bean, list);
		}
		list.add(DynamicVisibleBeanPropertyListener.class, listener);
	}

	public static void removeVisibleChangeListener(Object bean, DynamicVisibleBeanPropertyListener listener) {
		EventListenerList list = LISTENERS.get(bean);
		if (list == null)
			return;
		list.remove(DynamicVisibleBeanPropertyListener.class, listener);
	}

	default void fireSetPropertyVisible(DynamicVisibleBean bean, String propertyName, boolean visible) {
		FxUtils.runLaterIfNeeded(() -> {
			EventListenerList list = LISTENERS.get(bean);
			if (list == null)
				return;
			for (DynamicVisibleBeanPropertyListener listener : list.getListeners(DynamicVisibleBeanPropertyListener.class))
				listener.visiblePropertyChange(propertyName, visible);
		});
	}

	public void setVisible();
}
