/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.multiNumber.point;

import java.util.List;
import java.util.function.Supplier;

import javax.vecmath.Point4i;

import org.beanmanager.editors.multiNumber.StaticSizeMonoDimensionEditor;
import org.beanmanager.editors.primitive.number.IntegerEditor;
import org.beanmanager.editors.primitive.number.NumberEditor;

public class Point4iEditor extends StaticSizeMonoDimensionEditor<Point4i, Integer> {
	public Point4iEditor() {
		super(4);
	}

	@Override
	protected List<Integer> getArrayFromValue(Point4i value) {
		return List.of(value.x, value.y, value.z, value.w);
	}

	@Override
	protected Supplier<NumberEditor<Integer>> getEditorConstructor() {
		return IntegerEditor::new;
	}

	@Override
	protected Point4i getValueFromArray(List<Integer> datas) {
		return new Point4i(datas.get(0), datas.get(1), datas.get(2), datas.get(3));
	}
}
