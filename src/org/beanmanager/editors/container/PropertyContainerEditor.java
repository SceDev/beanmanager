/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.container;

import java.beans.PropertyDescriptor;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Supplier;

import org.beanmanager.BeanManager;
import org.beanmanager.editors.GenericPropertyEditor;
import org.beanmanager.editors.PropertyEditor;
import org.beanmanager.editors.PropertyEditorManager;

public abstract class PropertyContainerEditor<T> extends PropertyEditor<T> {
	protected final Class<?> componentType;
	protected String local;
	protected Function<Class<?>, PropertyEditor<?>> patternEditorFactory;
	private Object patternEditor;

	// public PropertyContainerEditor() {}
	//
	// public PropertyContainerEditor(String local) {
	// this.local = local;
	// }

	public PropertyContainerEditor(Class<?> componentType, String local) {
		Objects.requireNonNull(componentType, "Cannot create property container without type");
		Objects.requireNonNull(local, "Cannot create property container local");
		this.componentType = componentType;
		this.local = local;
	}

	// public abstract void beanListChanged(Class<? extends Object> type);

	@SuppressWarnings("unchecked")
	public PropertyEditor<?> getDefaultPatternEditor(Supplier<Class<?>[]> genericTypesProvider) {
		if (this.patternEditor == null) {
			PropertyEditor<?> patternEditor = getPropertyEditorClone(genericTypesProvider);
			if (patternEditor instanceof GenericPropertyEditor) {
				HashMap<List<Class<?>>, PropertyEditor<?>> patternEditorMap = new HashMap<>();
				patternEditorMap.put(Arrays.asList(((GenericPropertyEditor<?>) patternEditor).getTypes()), patternEditor);
				this.patternEditor = patternEditorMap;
			} else
				this.patternEditor = patternEditor;
			return patternEditor;
		} else if (this.patternEditor instanceof PropertyEditor)
			return (PropertyEditor<?>) this.patternEditor;
		else {
			PropertyEditor<?> e = ((HashMap<List<Class<?>>, PropertyEditor<?>>) this.patternEditor).get(Arrays.asList(genericTypesProvider.get()));
			if (e == null) {
				// System.out.println("create other generic editor");
				e = getPropertyEditorClone(genericTypesProvider);
				((HashMap<Class<?>[], PropertyEditor<?>>) this.patternEditor).put(((GenericPropertyEditor<?>) e).getTypes(), e);
			}
			return e;
		}
	}

	public Class<?> getComponentType() {
		return this.componentType;
	}

	protected PropertyEditor<?> getPropertyEditorClone(Supplier<Class<?>[]> genericTypesProvider) {
		return this.patternEditorFactory != null ? this.patternEditorFactory.apply(this.componentType) : PropertyEditorManager.findEditor(this.componentType, this.local, null, genericTypesProvider);
	}

	public abstract List<Object> getSubBeans();

	public void initPatternEditorFactory(Function<Class<?>, PropertyEditor<?>> patternEditorFactory) {
		this.patternEditorFactory = patternEditorFactory;
	}

	public abstract void removeSubBeans(Object beanToRemove);

//	public void setLocal(String local) {
//		this.local = local;
//	}

	@Override
	public boolean canContainForbiddenCharacter() {
		for (PropertyDescriptor pd : BeanManager.getPropertyDescriptors(this.componentType)) {
			PropertyEditor<?> e = PropertyEditorManager.findEditor(pd.getPropertyType(), this.local, null, () -> BeanManager.getGenericTypes(pd));
			if (e != null && e.canContainForbiddenCharacter())
				return true;
		}
		return false;
	}

	// public abstract void purgeModule(String moduleToRemoveName); //TODO j'en fait quoi???

	public abstract boolean isInline();
}
