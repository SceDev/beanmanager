/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors;

import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodHandles.Lookup;
import java.lang.invoke.MethodType;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.net.InetSocketAddress;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.BitSet;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.vecmath.Color4f;
import javax.vecmath.GMatrix;
import javax.vecmath.GVector;
import javax.vecmath.Matrix3d;
import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4d;
import javax.vecmath.Matrix4f;
import javax.vecmath.Point2d;
import javax.vecmath.Point2f;
import javax.vecmath.Point2i;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Point3i;
import javax.vecmath.Point4d;
import javax.vecmath.Point4f;
import javax.vecmath.Point4i;
import javax.vecmath.Vector2d;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4d;
import javax.vecmath.Vector4f;

import org.beanmanager.editors.basic.BitSetEditor;
import org.beanmanager.editors.basic.BitSetInfo;
import org.beanmanager.editors.basic.BooleanPropertyEditor;
import org.beanmanager.editors.basic.Color4fEditor;
import org.beanmanager.editors.basic.ColorEditor;
import org.beanmanager.editors.basic.DynamicBitSetInfo;
import org.beanmanager.editors.basic.DynamicPathInfo;
import org.beanmanager.editors.basic.DynamicStringInfo;
import org.beanmanager.editors.basic.EnumEditor;
import org.beanmanager.editors.basic.FileEditor;
import org.beanmanager.editors.basic.InetSocketAdressEditor;
import org.beanmanager.editors.basic.PathEditor;
import org.beanmanager.editors.basic.PathInfo;
import org.beanmanager.editors.basic.RunnableEditor;
import org.beanmanager.editors.basic.SelectionEditor;
import org.beanmanager.editors.basic.StringEditor;
import org.beanmanager.editors.basic.StringInfo;
import org.beanmanager.editors.container.ArrayEditor;
import org.beanmanager.editors.container.ArrayInfo;
import org.beanmanager.editors.container.BeanEditor;
import org.beanmanager.editors.container.BeanInfo;
import org.beanmanager.editors.container.DynamicBeanInfo;
import org.beanmanager.editors.container.PropertyContainerEditor;
import org.beanmanager.editors.container.TreeEditor;
import org.beanmanager.editors.multiNumber.matrix.DynamicMatrixInfo;
import org.beanmanager.editors.multiNumber.matrix.GMatrixEditor;
import org.beanmanager.editors.multiNumber.matrix.Matrix3dEditor;
import org.beanmanager.editors.multiNumber.matrix.Matrix3fEditor;
import org.beanmanager.editors.multiNumber.matrix.Matrix4dEditor;
import org.beanmanager.editors.multiNumber.matrix.Matrix4fEditor;
import org.beanmanager.editors.multiNumber.matrix.MatrixInfo;
import org.beanmanager.editors.multiNumber.point.Point2dEditor;
import org.beanmanager.editors.multiNumber.point.Point2fEditor;
import org.beanmanager.editors.multiNumber.point.Point2iEditor;
import org.beanmanager.editors.multiNumber.point.Point3dEditor;
import org.beanmanager.editors.multiNumber.point.Point3fEditor;
import org.beanmanager.editors.multiNumber.point.Point3iEditor;
import org.beanmanager.editors.multiNumber.point.Point4dEditor;
import org.beanmanager.editors.multiNumber.point.Point4fEditor;
import org.beanmanager.editors.multiNumber.point.Point4iEditor;
import org.beanmanager.editors.multiNumber.vector.GVectorEditor;
import org.beanmanager.editors.multiNumber.vector.Vector2dEditor;
import org.beanmanager.editors.multiNumber.vector.Vector2fEditor;
import org.beanmanager.editors.multiNumber.vector.Vector3dEditor;
import org.beanmanager.editors.multiNumber.vector.Vector3fEditor;
import org.beanmanager.editors.multiNumber.vector.Vector4dEditor;
import org.beanmanager.editors.multiNumber.vector.Vector4fEditor;
import org.beanmanager.editors.multiNumber.vector.VectorInfo;
import org.beanmanager.editors.primitive.BooleanEditor;
import org.beanmanager.editors.primitive.CharacterEditor;
import org.beanmanager.editors.primitive.PrimitiveEditor;
import org.beanmanager.editors.primitive.number.ByteEditor;
import org.beanmanager.editors.primitive.number.ControlType;
import org.beanmanager.editors.primitive.number.DoubleEditor;
import org.beanmanager.editors.primitive.number.DynamicNumberInfo;
import org.beanmanager.editors.primitive.number.FloatEditor;
import org.beanmanager.editors.primitive.number.IncrementMode;
import org.beanmanager.editors.primitive.number.IntegerEditor;
import org.beanmanager.editors.primitive.number.LongEditor;
import org.beanmanager.editors.primitive.number.NumberEditor;
import org.beanmanager.editors.primitive.number.NumberInfo;
import org.beanmanager.editors.primitive.number.ShortEditor;
import org.beanmanager.editors.time.DateEditor;
import org.beanmanager.editors.time.DateInfo;
import org.beanmanager.editors.time.DynamicDateInfo;
import org.beanmanager.editors.time.DynamicLocalTimeInfo;
import org.beanmanager.editors.time.LocalDateEditor;
import org.beanmanager.editors.time.LocalDateTimeEditor;
import org.beanmanager.editors.time.LocalTimeEditor;
import org.beanmanager.editors.time.LocalTimeInfo;
import org.beanmanager.struct.BooleanProperty;
import org.beanmanager.struct.Selection;
import org.beanmanager.struct.TreeRoot;
import org.beanmanager.tools.TriConsumer;
import org.beanmanager.tools.WrapperTools;

import javafx.scene.paint.Color;

@SuppressWarnings("unchecked")
public class PropertyEditorManager {
	private static final EditorMap EDITORS_MAP = new EditorMap();
	static {
		EDITORS_MAP.put(boolean.class, EditorDescriptor.build(BooleanEditor.class));
		EDITORS_MAP.put(Boolean.class, EditorDescriptor.build(BooleanEditor.class));
		EDITORS_MAP.put(byte.class,
				EditorDescriptor.build(ByteEditor.class).addAnnotation(NumberInfo.class, (editor, annotation, methodInvocator) -> populateNumberInfo(editor, annotation, methodInvocator))
						.addDynamicAnnotation(DynamicNumberInfo.class, (editor, annotation, methodInvocator) -> populateNumberInfo(editor, annotation, methodInvocator)));
		EDITORS_MAP.put(Byte.class,
				EditorDescriptor.build(ByteEditor.class).addAnnotation(NumberInfo.class, (editor, annotation, methodInvocator) -> populateNumberInfo(editor, annotation, methodInvocator))
						.addDynamicAnnotation(DynamicNumberInfo.class, (editor, annotation, methodInvocator) -> populateNumberInfo(editor, annotation, methodInvocator)));
		EDITORS_MAP.put(double.class,
				EditorDescriptor.build(DoubleEditor.class).addAnnotation(NumberInfo.class, (editor, annotation, methodInvocator) -> populateNumberInfo(editor, annotation, methodInvocator))
						.addDynamicAnnotation(DynamicNumberInfo.class, (editor, annotation, methodInvocator) -> populateNumberInfo(editor, annotation, methodInvocator)));
		EDITORS_MAP.put(Double.class,
				EditorDescriptor.build(DoubleEditor.class).addAnnotation(NumberInfo.class, (editor, annotation, methodInvocator) -> populateNumberInfo(editor, annotation, methodInvocator))
						.addDynamicAnnotation(DynamicNumberInfo.class, (editor, annotation, methodInvocator) -> populateNumberInfo(editor, annotation, methodInvocator)));
		EDITORS_MAP.put(float.class,
				EditorDescriptor.build(FloatEditor.class).addAnnotation(NumberInfo.class, (editor, annotation, methodInvocator) -> populateNumberInfo(editor, annotation, methodInvocator))
						.addDynamicAnnotation(DynamicNumberInfo.class, (editor, annotation, methodInvocator) -> populateNumberInfo(editor, annotation, methodInvocator)));
		EDITORS_MAP.put(Float.class,
				EditorDescriptor.build(FloatEditor.class).addAnnotation(NumberInfo.class, (editor, annotation, methodInvocator) -> populateNumberInfo(editor, annotation, methodInvocator))
						.addDynamicAnnotation(DynamicNumberInfo.class, (editor, annotation, methodInvocator) -> populateNumberInfo(editor, annotation, methodInvocator)));
		EDITORS_MAP.put(int.class,
				EditorDescriptor.build(IntegerEditor.class).addAnnotation(NumberInfo.class, (editor, annotation, methodInvocator) -> populateNumberInfo(editor, annotation, methodInvocator))
						.addDynamicAnnotation(DynamicNumberInfo.class, (editor, annotation, methodInvocator) -> populateNumberInfo(editor, annotation, methodInvocator)));
		EDITORS_MAP.put(Integer.class,
				EditorDescriptor.build(IntegerEditor.class).addAnnotation(NumberInfo.class, (editor, annotation, methodInvocator) -> populateNumberInfo(editor, annotation, methodInvocator))
						.addDynamicAnnotation(DynamicNumberInfo.class, (editor, annotation, methodInvocator) -> populateNumberInfo(editor, annotation, methodInvocator)));
		EDITORS_MAP.put(long.class,
				EditorDescriptor.build(LongEditor.class).addAnnotation(NumberInfo.class, (editor, annotation, methodInvocator) -> populateNumberInfo(editor, annotation, methodInvocator))
						.addDynamicAnnotation(DynamicNumberInfo.class, (editor, annotation, methodInvocator) -> populateNumberInfo(editor, annotation, methodInvocator)));
		EDITORS_MAP.put(Long.class,
				EditorDescriptor.build(LongEditor.class).addAnnotation(NumberInfo.class, (editor, annotation, methodInvocator) -> populateNumberInfo(editor, annotation, methodInvocator))
						.addDynamicAnnotation(DynamicNumberInfo.class, (editor, annotation, methodInvocator) -> populateNumberInfo(editor, annotation, methodInvocator)));
		EDITORS_MAP.put(short.class,
				EditorDescriptor.build(ShortEditor.class).addAnnotation(NumberInfo.class, (editor, annotation, methodInvocator) -> populateNumberInfo(editor, annotation, methodInvocator))
						.addDynamicAnnotation(DynamicNumberInfo.class, (editor, annotation, methodInvocator) -> populateNumberInfo(editor, annotation, methodInvocator)));
		EDITORS_MAP.put(Short.class,
				EditorDescriptor.build(ShortEditor.class).addAnnotation(NumberInfo.class, (editor, annotation, methodInvocator) -> populateNumberInfo(editor, annotation, methodInvocator))
						.addDynamicAnnotation(DynamicNumberInfo.class, (editor, annotation, methodInvocator) -> populateNumberInfo(editor, annotation, methodInvocator)));
		EDITORS_MAP.put(char.class, EditorDescriptor.build(CharacterEditor.class));
		EDITORS_MAP.put(Character.class, EditorDescriptor.build(CharacterEditor.class));

		EDITORS_MAP.put(String.class, EditorDescriptor.build(StringEditor.class).addAnnotation(StringInfo.class, (editor, annotation, methodInvocator) -> {
			editor.setMaxLength(annotation.maxLength());
			editor.setPromptText(annotation.promptText());
		}).addDynamicAnnotation(DynamicStringInfo.class, (editor, anno, methodInvocator) -> {
			Object obj;
			if ((obj = getObject(methodInvocator, anno.maxLengthMethodName(), Integer.class)) != null)
				editor.setMaxLength((int) obj);
			if ((obj = getObject(methodInvocator, anno.promptTextMethodName(), String.class)) != null)
				editor.setPromptText((String) obj);
		}));
		EDITORS_MAP.put(File.class, EditorDescriptor.build(FileEditor.class).addAnnotation(PathInfo.class, (editor, anno, methodInvocator) -> {
			editor.setDirectory(anno.directory());
			editor.setFilters(anno.filters());
		}).addDynamicAnnotation(DynamicPathInfo.class, (editor, anno, methodInvocator) -> {
			Object obj;
			if ((obj = getObject(methodInvocator, anno.directoryMethodName(), Boolean.class)) != null)
				editor.setDirectory((boolean) obj);
			if ((obj = getObject(methodInvocator, anno.filtersMethodName(), String[].class)) != null)
				editor.setFilters((String[]) obj);
		}));
		EDITORS_MAP.put(Path.class, EditorDescriptor.build(PathEditor.class).addAnnotation(PathInfo.class, (editor, anno, methodInvocator) -> {
			editor.setDirectory(anno.directory());
			editor.setFilters(anno.filters());
		}).addDynamicAnnotation(DynamicPathInfo.class, (editor, anno, methodInvocator) -> {
			Object obj;
			if ((obj = getObject(methodInvocator, anno.directoryMethodName(), Boolean.class)) != null)
				editor.setDirectory((boolean) obj);
			if ((obj = getObject(methodInvocator, anno.filtersMethodName(), String[].class)) != null)
				editor.setFilters((String[]) obj);
		}));
		EDITORS_MAP.put(InetSocketAddress.class, EditorDescriptor.build(InetSocketAdressEditor.class));
		// editorsMap.put(Object.class, ObjectEditor.class); //Non!!!
		EDITORS_MAP.put(Point2i.class, EditorDescriptor.build(Point2iEditor.class));
		EDITORS_MAP.put(Point2f.class, EditorDescriptor.build(Point2fEditor.class));
		EDITORS_MAP.put(Point2d.class, EditorDescriptor.build(Point2dEditor.class));
		EDITORS_MAP.put(Point3i.class, EditorDescriptor.build(Point3iEditor.class));
		EDITORS_MAP.put(Point3f.class, EditorDescriptor.build(Point3fEditor.class));
		EDITORS_MAP.put(Point3d.class, EditorDescriptor.build(Point3dEditor.class));
		EDITORS_MAP.put(Point4i.class, EditorDescriptor.build(Point4iEditor.class));
		EDITORS_MAP.put(Point4f.class, EditorDescriptor.build(Point4fEditor.class));
		EDITORS_MAP.put(Point4d.class, EditorDescriptor.build(Point4dEditor.class));
		EDITORS_MAP.put(Vector2f.class, EditorDescriptor.build(Vector2fEditor.class));
		EDITORS_MAP.put(Vector2d.class, EditorDescriptor.build(Vector2dEditor.class));
		EDITORS_MAP.put(Vector3f.class, EditorDescriptor.build(Vector3fEditor.class));
		EDITORS_MAP.put(Vector3d.class, EditorDescriptor.build(Vector3dEditor.class));
		EDITORS_MAP.put(Vector4f.class, EditorDescriptor.build(Vector4fEditor.class));
		EDITORS_MAP.put(Vector4d.class, EditorDescriptor.build(Vector4dEditor.class));
		EDITORS_MAP.put(GMatrix.class, EditorDescriptor.build(GMatrixEditor.class));
		EDITORS_MAP.put(Matrix3f.class, EditorDescriptor.build(Matrix3fEditor.class));
		EDITORS_MAP.put(Matrix3d.class, EditorDescriptor.build(Matrix3dEditor.class));
		EDITORS_MAP.put(Matrix4f.class, EditorDescriptor.build(Matrix4fEditor.class));
		EDITORS_MAP.put(Matrix4d.class, EditorDescriptor.build(Matrix4dEditor.class));
		EDITORS_MAP.put(Color.class, EditorDescriptor.build(ColorEditor.class));
		EDITORS_MAP.put(Color4f.class, EditorDescriptor.build(Color4fEditor.class));
		EDITORS_MAP.put(BitSet.class, EditorDescriptor.build(BitSetEditor.class).addAnnotation(BitSetInfo.class, (editor, anno, methodInvocator) -> {
			editor.setMinSize(anno.minSize());
			editor.setMaxSize(anno.maxSize());
		}).addDynamicAnnotation(DynamicBitSetInfo.class, (editor, anno, methodInvocator) -> {
			Object minSize;
			Object maxSize;
			if ((minSize = getObject(methodInvocator, anno.minSizeMethodName(), Integer.class)) != null && (maxSize = getObject(methodInvocator, anno.maxSizeMethodName(), Integer.class)) != null) {
				editor.setMinSize((int) minSize);
				editor.setMaxSize((int) maxSize);
			}
		}));
		EDITORS_MAP.put(Selection.class, EditorDescriptor.build(SelectionEditor.class));
		EDITORS_MAP.put(TreeRoot.class, EditorDescriptor.build(TreeEditor.class));
		EDITORS_MAP.put(BooleanProperty.class, EditorDescriptor.build(BooleanPropertyEditor.class));
		EDITORS_MAP.put(Matrix4f.class, EditorDescriptor.build(Matrix4fEditor.class));
		EDITORS_MAP.put(Matrix4d.class, EditorDescriptor.build(Matrix4dEditor.class));
		EDITORS_MAP.put(GMatrix.class, EditorDescriptor.build(GMatrixEditor.class).addAnnotation(MatrixInfo.class, (editor, anno, methodInvocator) -> {
			editor.updateSizes(anno.nRow(), anno.nCol());
		}).addDynamicAnnotation(DynamicMatrixInfo.class, (editor, anno, methodInvocator) -> {
			Object nCol;
			Object nRow;
			if ((nCol = getObject(methodInvocator, anno.nColMethodName(), Integer.class)) != null && (nRow = getObject(methodInvocator, anno.nRowMethodName(), Integer.class)) != null)
				editor.updateSizes((Integer) nRow, (Integer) nCol);
		}));
		EDITORS_MAP.put(GVector.class, EditorDescriptor.build(GVectorEditor.class).addAnnotation(VectorInfo.class, (editor, anno, methodInvocator) -> {
			editor.updateSize(anno.size());
		}).addDynamicAnnotation(DynamicMatrixInfo.class, (editor, anno, methodInvocator) -> {
			Object size;
			if ((size = getObject(methodInvocator, anno.nColMethodName(), Integer.class)) != null)
				editor.updateSize((Integer) size);
		}));
		EDITORS_MAP.put(Runnable.class, EditorDescriptor.build(RunnableEditor.class));
		EDITORS_MAP.put(LocalDate.class, EditorDescriptor.build(LocalDateEditor.class));
		EDITORS_MAP.put(LocalTime.class, EditorDescriptor.build(LocalTimeEditor.class).addAnnotation(LocalTimeInfo.class, (editor, anno, methodInvocator) -> {
			editor.setFilter(anno.hour(), anno.minute(), anno.second(), anno.nanosecond());
		}).addDynamicAnnotation(DynamicLocalTimeInfo.class, (editor, anno, methodInvocator) -> {
			editor.setFilter((boolean) getObject(methodInvocator, anno.hourMethodName(), Boolean.class), (Boolean) getObject(methodInvocator, anno.minuteMethodName(), Boolean.class),
					(Boolean) getObject(methodInvocator, anno.secondMethodName(), Boolean.class), (Boolean) getObject(methodInvocator, anno.nanosecondMethodName(), Boolean.class));
		}));
		EDITORS_MAP.put(LocalDateTime.class, EditorDescriptor.build(LocalDateTimeEditor.class).addAnnotation(LocalTimeInfo.class,
				(editor, anno, methodInvocator) -> editor.setFilter(anno.hour(), anno.minute(), anno.second(), anno.nanosecond())));
		EDITORS_MAP.put(Date.class, EditorDescriptor.build(DateEditor.class).addAnnotation(DateInfo.class, (editor, anno, methodInvocator) -> {
			editor.setMin(anno.min());
			editor.setMax(anno.max());
			editor.setTimePattern(anno.timePattern());
		}).addDynamicAnnotation(DynamicDateInfo.class, (editor, anno, methodInvocator) -> {
			Object obj;
			if ((obj = getObject(methodInvocator, anno.minMethodName(), Long.class)) != null)
				editor.setMin((Long) obj);
			if ((obj = getObject(methodInvocator, anno.maxMethodName(), Long.class)) != null)
				editor.setMax((Long) obj);
			editor.setTimePattern((String) getObject(methodInvocator, anno.timePatternMethodName(), String.class));
		}));
	}

	private PropertyEditorManager() {}

	private static <U extends Number> void populateNumberInfo(NumberEditor<U> editor, Annotation anno, Function<String, Object> methodInvocator) {
		if (anno instanceof NumberInfo) {
			NumberInfo fni = (NumberInfo) anno;
			editor.setMin(Double.isNaN(fni.min()) ? null : editor.getNumberAsGeneric(fni.min()));
			editor.setMax(Double.isNaN(fni.max()) ? null : editor.getNumberAsGeneric(fni.max()));
			editor.setIncrement(fni.increment());
			editor.setIncrementMode(fni.incrementMode());
			editor.setControleType(fni.controlType());
		} else if (anno instanceof DynamicNumberInfo) {
			DynamicNumberInfo dni = (DynamicNumberInfo) anno;
			try {
				Object obj;
				if ((obj = getObject(methodInvocator, dni.controlTypeMethodName(), ControlType.class)) != null)
					editor.setControleType((ControlType) obj);
				if ((obj = getObject(methodInvocator, dni.incrementMethodName(), Double.class)) != null)
					editor.setIncrement((Double) obj);
				if ((obj = getObject(methodInvocator, dni.incrementModeMethodName(), IncrementMode.class)) != null)
					editor.setIncrementMode((IncrementMode) obj);
				if ((obj = getObject(methodInvocator, dni.minMethodName(), Double.class)) != null)
					editor.setMin(editor.getNumberAsGeneric((Double) obj));
				if ((obj = getObject(methodInvocator, dni.maxMethodName(), Double.class)) != null)
					editor.setMax(editor.getNumberAsGeneric((Double) obj));
			} catch (IllegalArgumentException | SecurityException e) {
				e.printStackTrace();
			}
		}
	}

	private static Object getObject(Function<String, Object> methodInvocator, String methodName, Class<?> type) {
		if (methodName.isEmpty())
			return null;
		Object obj = methodInvocator.apply(methodName);
		if (obj != null && obj.getClass() == type)
			return obj;
		else if (type.isPrimitive())
			System.err.println("Invocation of the method: " + methodName + " of the " + DynamicNumberInfo.class.getSimpleName() + " return: "
					+ (obj == null ? "null" : obj.getClass().getSimpleName() + " instead of: " + type.getSimpleName()));
		return null;
	}

	public static <T> PropertyEditor<T> findEditor(Class<T> type, String local) {
		return findEditor(type, local, null);
	}

	public static <T> PropertyEditor<T> findEditor(Class<T> type, String local, Function<Class<?>, PropertyEditor<?>> patternEditorFactory) {
		return findEditor(type, local, patternEditorFactory, null, null, null, null);
	}

	public static <T> PropertyEditor<T> findEditor(Class<T> type, String local, Function<Class<?>, PropertyEditor<?>> patternEditorFactory, Supplier<Class<?>[]> genericTypesProvider) {
		return findEditor(type, local, patternEditorFactory, genericTypesProvider, null, null, null);
	}

	public static <T> PropertyEditor<T> findEditor(Class<T> type, String local, Function<Class<?>, PropertyEditor<?>> patternEditorFactory, Supplier<Class<?>[]> genericTypesProvider,
			Annotation[] anno, Function<String, Object> methodInvocator) {
		return findEditor(type, local, patternEditorFactory, genericTypesProvider, null, anno, methodInvocator);
	}

	@SuppressWarnings("rawtypes")
	private static <T> PropertyEditor<T> findEditor(Class<T> type, String local, Function<Class<?>, PropertyEditor<?>> patternEditorFactory, Supplier<Class<?>[]> genericTypesProvider, T value,
			Annotation[] annos, Function<String, Object> methodInvocator) {
		if (type == null)
			return null;
		if (type.isEnum())
			return new EnumEditor(type);
		EditorDescriptor<T, ?> editorDescriptor = EDITORS_MAP.get(type);
		PropertyEditor<T> propertyEditor = null;
		if (editorDescriptor != null) {
			Class<? extends PropertyEditor<T>> editorType = editorDescriptor.editorClass;
			try {
				try {
					propertyEditor = editorType.getConstructor().newInstance();
				} catch (NoSuchMethodException e) {
					if (genericTypesProvider != null) {
						Class<?>[] genericTypes = genericTypesProvider.get();
						if(GenericPropertyEditor.class.isAssignableFrom(editorType))
							propertyEditor = editorType.getConstructor(Class.class).newInstance(genericTypes[0]);
						else		// PropertyContainerEditor
							propertyEditor = editorType.getConstructor(Class.class, String.class).newInstance(genericTypes[0], local);
					} else if (value != null)
						propertyEditor = editorType.getConstructor(type).newInstance(value);
					else
						// System.err.println("No empty constructor for the editor: " + editorType);
						return null;
				}
				if (propertyEditor instanceof PropertyContainerEditor<?>) {
//					((PropertyContainerEditor<T>) propertyEditor).setLocal(local);
					((PropertyContainerEditor<T>) propertyEditor).initPatternEditorFactory(patternEditorFactory);
				}
				if (propertyEditor instanceof PrimitiveEditor)
					((PrimitiveEditor<?>) propertyEditor).setPrimitive(WrapperTools.PRIMITIVES.contains(type));
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
				System.err.println("Cannot instantiate the editor: " + editorType.getSimpleName() + ", Does this class have an empty constructor?");
				e.printStackTrace();
				return null;
			}
		} else if (type.isArray() && findEditor(ArrayEditor.getComponentType(type), local, null, () -> {
			Boolean hasEditor = hasEditor(value);
			return value == null || !value.getClass().isArray() ? null : hasEditor == null || hasEditor ? new Class<?>[] { Void.class } : null;
		}, annos, methodInvocator) != null) {
			ArrayEditor<T> arrayEditor = new ArrayEditor<>(type, local);
			arrayEditor.initPatternEditorFactory(patternEditorFactory);
//			arrayEditor.setLocal(local);
			propertyEditor = arrayEditor;
		} else {
			// Potentiellement un BeanEditor
			boolean inline = false;
			Class<?>[] subClasses = null;
			BeanInfo beanInfoAnno = null;
			DynamicBeanInfo dynamicBeanInfoAnno = null;
			if (annos != null) {
				for (Annotation anno : annos)
					if (anno instanceof DynamicBeanInfo) {
						dynamicBeanInfoAnno = (DynamicBeanInfo) anno;
						Class<?>[] subTypes = (Class<?>[]) methodInvocator.apply(dynamicBeanInfoAnno.possibleSubclassesMethodName());
						if (subTypes != null && subTypes.length != 0)
							subClasses = subTypes;
						break;
					}
				for (Annotation anno : annos)
					if (anno instanceof BeanInfo) {
						beanInfoAnno = (BeanInfo) anno;
						if (subClasses == null) {
							Class<?>[] subTypes = beanInfoAnno.possibleSubclasses();
							if (subTypes.length != 0)
								subClasses = subTypes;
						}
						inline = beanInfoAnno.inline();
						break;
					}
			}
			if (hadPublicDefaultConstructor(type)/* || type.isInterface() */) // Bug avant pk une interface est tjs ok???
				propertyEditor = new BeanEditor<>(type, local, inline, subClasses);
			else if (subClasses != null) { // J'ai tjs pas d'éditeur, je cherche dans les sous-type s'ils existent
				for (Class<?> subType : subClasses)
					if (PropertyEditorManager.findEditor(subType, local, patternEditorFactory, genericTypesProvider) == null)
						return null;
				// Des sous types existes, vérifier qu'ils soient compatible avec le type
				boolean isAllAssignable = true;
				for (Class<?> subType : subClasses)
					if (!type.isAssignableFrom(subType)) {
						isAllAssignable = false;
						break;
					}
				if (isAllAssignable)
					propertyEditor = new BeanEditor<>(type, local, inline, subClasses);
				else {

				}
			}
		}
//		if (propertyEditor == null) {		//Pas bon, sinon testbean.inheritance marche pas...
//			// No editor, try to found an editor for one of super class
//			Class<?> testedClass = type.getSuperclass();
//			while (testedClass != Object.class && testedClass != null) {
//				PropertyEditor<?> pe = findEditor(testedClass, local, patternEditorFactory, genericTypesProvider, annos, methodInvocator);
//				if (pe != null) {
//					propertyEditor = (PropertyEditor<T>) pe;
//					break;
//				}
//				testedClass = testedClass.getSuperclass();
//			}
//			// No editor, try to found an editor in interfaces for the given class or for one of super class
//			testedClass = type;
//			editorFound: while (testedClass != Object.class && testedClass != null) {
//				for (Class<?> i : testedClass.getInterfaces()) {
//					PropertyEditor<?> pe = findEditor(i, local, patternEditorFactory, genericTypesProvider, annos, methodInvocator);
//					if (pe != null) {
//						propertyEditor = (PropertyEditor<T>) pe;
//						break editorFound;
//					}
//				}
//				testedClass = testedClass.getSuperclass();
//			}
//		}
		if (propertyEditor != null && annos != null) {
			if (editorDescriptor != null)
				editorDescriptor.apply(propertyEditor, annos, methodInvocator);
			else if (propertyEditor instanceof ArrayEditor)
				for (Annotation anno : annos)
					if (anno instanceof ArrayInfo) {
						((ArrayEditor<?>) propertyEditor).setMinHeight(((ArrayInfo) anno).minHeight());
						((ArrayEditor<?>) propertyEditor).setPrefHeight(((ArrayInfo) anno).prefHeight());
						((ArrayEditor<?>) propertyEditor).setMaxHeight(((ArrayInfo) anno).maxHeight());
						break;
					}
			for (Annotation anno : annos)
				if (anno instanceof PropertyInfo) {
					propertyEditor.setNullable(((PropertyInfo) anno).nullable());
					break;
				}
		}
		return propertyEditor;
	}

	public static void updateDynamicAnnotationEditorProperties(Class<?> beanClass, PropertyEditor<?> propertyEditor, Annotation[] annotations, Function<String, Object> methodInvocator) {
		EditorDescriptor<?, ?> ed = EDITORS_MAP.get(beanClass);
		if (ed == null) {
			if (propertyEditor instanceof BeanEditor)
				for (Annotation anno : annotations)
					if (anno instanceof DynamicBeanInfo) {
						((BeanEditor<?>) propertyEditor).setSubClasses((Class<?>[]) methodInvocator.apply(((DynamicBeanInfo) anno).possibleSubclassesMethodName()));
						break;
					}
		} else
			ed.applyDynamic(propertyEditor, annotations, methodInvocator);
	}

	private static Boolean hasEditor(Object value) {
		int length = Array.getLength(value);
		if (length == 0)
			return null;
		for (int i = 0; i < length; i++) {
			Object subValue = Array.get(value, i);
			if (subValue != null)
				if (subValue.getClass().isArray()) {
					Boolean res = hasEditor(subValue);
					if (res != null)
						return res;
				} else
					return findEditorWithValue(subValue, "") != null;
		}
		return null;
	}

	public static <T> PropertyEditor<T> findEditorWithValue(T value, String local) {
		return findEditor((Class<T>) value.getClass(), local, null, null, value, null, null);
	}

	public static void forEachEditor(BiConsumer<Class<?>, Class<? extends PropertyEditor<?>>> consumer) {
		EDITORS_MAP.forEach((type, ed) -> consumer.accept(type, ed.editorClass));
	}

	public static boolean hadPublicDefaultConstructor(Class<?> type) {
		if (Modifier.isAbstract(type.getModifiers()))
			return false;
		for (Constructor<?> constructor : type.getConstructors())
			if (constructor.getParameterTypes().length == 0 || type.isMemberClass() && constructor.getParameterTypes().length == 1)
				return true;
		return false;
	}

	public static void main(String[] args) throws Throwable {
		registerEditor(String.class, EditorDescriptor.build(StringEditor.class).addAnnotation(StringInfo.class, (editor, annotation, methodInvocator) -> {
			editor.setMaxLength(annotation.maxLength());
		}));
		Lookup lookup = MethodHandles.lookup();
		MethodHandle constructor = lookup.findConstructor(BooleanEditor.class, MethodType.methodType(void.class));
		BooleanEditor t = (BooleanEditor) constructor.invoke();
		System.out.println(t);
		registerEditor(Byte.class, ByteEditor.class);
	}

	public static <T, U extends PropertyEditor<T>> void registerEditor(Class<T> typeClass, Class<U> editorClass) {
		EDITORS_MAP.put(typeClass, EditorDescriptor.build(editorClass));
	}

	public static <T, U extends PropertyEditor<T>> void registerEditor(Class<T> typeClass, EditorDescriptor<T, U> editorDescriptor) {
		EDITORS_MAP.put(typeClass, editorDescriptor);
	}

	public static void purgeEditors(Module moduleToRemove) {
		EDITORS_MAP.values().removeIf(editorDescriptor -> editorDescriptor.editorClass.getModule().equals(moduleToRemove));
	}
}

class ClassToTriConsumerMap<U extends PropertyEditor<?>, V extends Function<String, Object>> {
	private final HashMap<Class<?>, TriConsumer<U, Object, V>> map = new HashMap<>();

	public <T> TriConsumer<U, ? super T, V> get(Class<T> key) {
		return this.map.get(key);
	}

	public Set<Class<?>> keyset() {
		return this.map.keySet();
	}

	public <T> TriConsumer<U, ? super T, V> put(Class<T> key, TriConsumer<U, ? super T, V> value) {
		return this.map.put(key, (o, u, p) -> value.accept(o, key.cast(u), p));
	}
}
