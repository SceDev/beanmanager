/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager.editors.time;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.time.LocalTime;

import org.beanmanager.editors.PropertyEditor;
import org.beanmanager.editors.primitive.number.ControlType;
import org.beanmanager.editors.primitive.number.IntegerEditor;
import org.beanmanager.ihmtest.FxTest;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

public class LocalTimeEditor extends PropertyEditor<LocalTime> {
	private IntegerEditor hourEditor;
	private IntegerEditor minuteEditor;
	private IntegerEditor secondEditor;
	private IntegerEditor nanoEditor;
	private boolean hourFilter = true;
	private boolean minuteFilter = true;
	private boolean secondFilter = true;
	private boolean nanosecondFilter = true;

	public static void main(String[] args) {

		LocalTimeEditor e1 = new LocalTimeEditor(false, true, true, false);
		new Thread(() -> {
			try {
				Thread.sleep(3000);
				Platform.runLater(() -> e1.setFilter(true, true, true, true));
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}
		}).start();

		new FxTest().launchIHM(args, s -> new VBox(e1.getEditor(), new LocalTimeEditor().getEditor(), new LocalTimeEditor(false, true, true, false).getEditor()));
	}

	public LocalTimeEditor() {}

	public LocalTimeEditor(boolean hoor, boolean minute, boolean second, boolean nanosecond) {
		setFilter(hoor, minute, second, nanosecond);
	}

	@Override
	public String getAsText() {
		return getValue() == null ? "" : getValue().toString();
	}

	@Override
	protected Region getCustomEditor() {
		HBox box = new HBox();
		populateBox(box);
		box.setAlignment(Pos.CENTER);
		box.setSpacing(2);
		box.setMaxWidth(Double.MAX_VALUE);
		updateCustomEditor();
		return box;
	}

	private void populateBox(HBox box) {
		ObservableList<Node> boxChildren = box.getChildren();
		boxChildren.clear();
		if (this.hourFilter) {
			this.hourEditor = new IntegerEditor(0, 23, ControlType.SPINNER);
			this.hourEditor.addPropertyChangeListener(() -> updateValue());
			Region hourControl = this.hourEditor.getNoSelectionEditor();
			hourControl.setPrefWidth(62);
			HBox.setHgrow(hourControl, Priority.ALWAYS);
			boxChildren.addAll(hourControl, new Label("h"));
		}
		if (this.minuteFilter) {
			this.minuteEditor = new IntegerEditor(0, 59, ControlType.SPINNER);
			this.minuteEditor.addPropertyChangeListener(() -> updateValue());
			Region minuteControl = this.minuteEditor.getNoSelectionEditor();
			minuteControl.setPrefWidth(62);
			HBox.setHgrow(minuteControl, Priority.ALWAYS);
			boxChildren.addAll(minuteControl, new Label("m"));
		}
		if (this.secondFilter) {
			this.secondEditor = new IntegerEditor(0, 59, ControlType.SPINNER);
			this.secondEditor.addPropertyChangeListener(() -> updateValue());
			Region secondControl = this.secondEditor.getNoSelectionEditor();
			secondControl.setPrefWidth(62);
			HBox.setHgrow(secondControl, Priority.ALWAYS);
			boxChildren.addAll(secondControl, new Label("s"));
		}
		if (this.nanosecondFilter) {
			this.nanoEditor = new IntegerEditor(0, 999_999_999, ControlType.SPINNER);
			this.nanoEditor.addPropertyChangeListener(() -> updateValue());
			Region nanoControl = this.nanoEditor.getNoSelectionEditor();
			nanoControl.setPrefWidth(120);
			HBox.setHgrow(nanoControl, Priority.ALWAYS);
			boxChildren.addAll(nanoControl, new Label("ns"));
		}
	}

	@Override
	public int getPitch() {
		return Byte.BYTES * 3 + Integer.BYTES;
	}

	@Override
	public boolean hasCustomEditor() {
		return true;
	}

	@Override
	public boolean isFixedControlSized() {
		return true;
	}

	@Override
	public LocalTime readValue(DataInput raf) throws IOException {
		return LocalTime.of(raf.readByte(), raf.readByte(), raf.readByte(), raf.readInt());
	}

	@Override
	public void setAsText(String text) {
		setValue(text == null || text.isEmpty() ? null : LocalTime.parse(text));
	}

	public void setFilter(boolean hour, boolean minute, boolean second, boolean nanosecond) {
		this.hourFilter = hour;
		this.minuteFilter = minute;
		this.secondFilter = second;
		this.nanosecondFilter = nanosecond;
		HBox hBox = (HBox) customEditor();
		if (hBox != null)
			populateBox(hBox);
	}

	@Override
	public void updateCustomEditor() {
		if (this.hourEditor == null)
			return;
		LocalTime time = getValue();
		if (time == null) {
			if (this.hourEditor.getValue() != null)
				this.hourEditor.setValue(null);
			if (this.minuteEditor.getValue() != null)
				this.minuteEditor.setValue(null);
			if (this.secondEditor.getValue() != null)
				this.secondEditor.setValue(null);
			if (this.nanoEditor.getValue() != null)
				this.nanoEditor.setValue(null);
		} else {
			if (this.hourEditor != null && this.hourEditor.getValue() != time.getHour())
				this.hourEditor.setValue(time.getHour());
			if (this.minuteEditor != null && this.minuteEditor.getValue() != time.getMinute())
				this.minuteEditor.setValue(time.getMinute());
			if (this.secondEditor != null && this.secondEditor.getValue() != time.getSecond())
				this.secondEditor.setValue(time.getSecond());
			if (this.nanoEditor != null && this.nanoEditor.getValue() != time.getNano())
				this.nanoEditor.setValue(time.getNano());
		}
	}

	private void updateValue() {
		super.setValue(LocalTime.of(this.hourEditor == null ? 0 : (int) this.hourEditor.getValue(), this.minuteEditor == null ? 0 : (int) this.minuteEditor.getValue(),
				this.secondEditor == null ? 0 : (int) this.secondEditor.getValue(), this.nanoEditor == null ? 0 : (int) this.nanoEditor.getValue()));
	}

	@Override
	public void writeValue(DataOutput raf, LocalTime value) throws IOException {
		raf.writeByte(value.getHour());
		raf.writeByte(value.getMinute());
		raf.writeByte(value.getSecond());
		raf.writeInt(value.getNano());
	}
}
