/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.beanmanager;

import java.util.EventListener;

@FunctionalInterface
public interface BeanRenameListener extends EventListener {
	void beanRename(BeanDesc<?> oldBeanDesc, BeanDesc<?> beanDesc);
}
