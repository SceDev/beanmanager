/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package unittest.inlineproperties;

import java.util.Objects;

import org.beanmanager.editors.container.BeanInfo;

public class A {
	@BeanInfo(inline = true)
	private B b = new B();
	private int u = 5;

	public A() {}

	public A(B b, int u) {
		this.b = b;
		this.u = u;
	}

	public B getB() {
		return this.b;
	}

	public void setB(B b) {
		this.b = b;
	}

	public int getU() {
		return this.u;
	}

	public void setU(int u) {
		this.u = u;
	}

	@Override
	public boolean equals(Object obj) {
		return this.b.equals(((A) obj).b) && this.u == ((A) obj).u;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.b, this.u);
	}
}
